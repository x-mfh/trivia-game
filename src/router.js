import Vue from 'vue';
import VueRouter from 'vue-router';
import store from './store/index'

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'start',
        component: () => import(/* webPackChunkName: "Start"*/ './components/Start/Start')
    },
    {
        path: '/quiz',
        name: 'quiz',
        component: () => import(/* webPackChunkName: "Question"*/ './components/Question/Question'),
        meta: { gameStart: true }
    },
    {
        path: '/result',
        name: 'result',
        component: () => import(/* webPackChunkName: "Result"*/ './components/Result/Result'),
        meta: { gameEnd: true }
    },
    {
        path: '*',
        component: () => {
            router.push('/')
            return import('./components/Start/Start')
        }
    }
];

const router = new VueRouter({ routes });

// Navigation guards to prevent going to /quiz or /result before it's intended
router.beforeEach((to, from, next) => {
    if(to.matched.some(record => record.meta.gameStart)) {
        if(store.state.gameState !== 'quiz') {
            next({
                name: "start"
            })
        } else {
            next();
        }
    } else if(to.matched.some(record => record.meta.gameEnd)) {
        if(store.state.gameState !== 'result') {
            next({
                name: "start"
            })
        } else {
            next();
        }
    } else {
        next()
    }
});

export default router;