import Vue from 'vue';
import Vuex from 'vuex';
import router from '../router'
import shuffleAnswers from '../utils/shuffleAnswers'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        selectedSettings: {
            difficulty: '',
            amountOfQuestions: '',
            categoryId: ''
        },
        categories: [],
        gameState: 'start',
        currentQuestion: 0,
        questions: [],
        fetching: false,
        fetchingError: ''
    },
    mutations: {
        setFetching: (state, payload) => {
            state.fetching = payload;
        },
        setFetchingError: (state, payload) => {
            state.fetchingError = payload;
        },
        setCategories: (state, payload) => {
            state.categories = payload;
        },
        setQuestions: (state, payload) => {
            state.questions = payload;
        },
        setCurrentQuestion: (state, payload) => {
            state.currentQuestion = payload;
        },
        setSelectedSettings: (state, payload) => {
            state.selectedSettings = payload;
        },
        setGameState: (state, payload) => {
            state.gameState = payload;
        },
        setQuestionAnswer: (state, payload) => {
            state.questions[state.currentQuestion].answer = payload;
            state.currentQuestion += 1;
            if(state.questions.length <= state.currentQuestion) {
                state.gameState = 'result'
                router.push('/result')
            }
        }
    },
    actions: {
        async fetchCategories({ commit }) {
            try {
                commit('setFetching', true);
                commit('setGameState', 'start')
                const response = await fetch('https://opentdb.com/api_category.php')
                const {trivia_categories} = await response.json();
                commit('setCategories', trivia_categories)
                const settings = {
                    difficulty: 'easy',
                    amountOfQuestions: '10',
                    categoryId: trivia_categories[0].id
                };
                commit('setSelectedSettings', settings)
            } catch (error) {
                commit('setFetchingError', error);
            } finally {
                commit('setFetching', false);
            }
        },

        async fetchQuestions({ commit, state }) {
            try {
                commit('setFetching', true);
                const response = 
                    await fetch(`https://opentdb.com/api.php?amount=${state.selectedSettings.amountOfQuestions}&category=${state.selectedSettings.categoryId}&difficulty=${state.selectedSettings.difficulty}`)
                const { results } = await response.json();
                const mappedQuestion = results.map((qst, i) => {
                    return {
                        id: i,
                        type : qst.type,
                        question: qst.question,
                        correct_answer: qst.correct_answer,
                        answers: shuffleAnswers([
                            ...qst.incorrect_answers,
                            qst.correct_answer
                        ])
                    }
                })
                commit('setQuestions', mappedQuestion)
                commit('setCurrentQuestion', 0)
                commit('setGameState', 'quiz')
                router.push('/quiz')
            } catch (error) {
                commit('setFetchingError', error);
                commit('setGameState', 'start')
            } finally {
                commit('setFetching', false);
            }
        }
    },
    getters: {
        numberOfQuestions: (state) => {
            return state.questions.length;
        },
        getQuestion: (state) => {
            if(state.questions.length >= state.currentQuestion)
                return state.questions[state.currentQuestion];
        },
        getResult: (state) => {
            let score = 0;
            let maxScore = 0;
            state.questions.forEach(q => {
                if(q.correct_answer === q.answer) {
                    score += 10;
                }
                maxScore += 10;
            })
            return { score, maxScore, answers: state.questions }
        }
    }
})