# trivia-game - Vue assignment

Build with vue, vue-router and vuex.

## deployed with heroku
https://mh-trivia-app.herokuapp.com/#/

Be careful with selecting too high a number of questions.
Recommended browser Chrome.

If UI scales are not matching your screen, try adjusting the zoom with Ctrl + MouseScroll.
Recommend 90% zoom on laptops.

### Todo:

- Make general styling classes in App.vue, to prevent having lots of duplicate styling (border, border-radius, padding, etc..)
  - Make general styling for wrapper classes
  - Make general styling for component titles
  - Make general styling for buttons /w pseudo-classes aswell (hover, active, etc...)
  - Make styling for switching answer padding for true/false and multiple answers 


- Fix margin and padding on different screen sizes

- Fix Question component receiving an undefined question on first run through due to lazy-load of Result component.

- Find better way to setup navigation guards in router.js

- Find way to prevent user from trying to get negative numbers of questions or more questions than available for category and difficulty (use https://opentdb.com/api_count.php?category=CATEGORY_ID_HERE)